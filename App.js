import 'react-native-gesture-handler';
import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import MainHeader from './src/components/MainHeader';
import Menu from './src/screens/Menu';
import Home from './src/screens/Home';
import MainBottom from './src/components/MainBottom';
const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            header: () => <MainHeader />,
          }}>
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="Menu" component={Menu} />
        </Stack.Navigator>
      </NavigationContainer>
      <MainBottom />
    </>
  );
};
export default App;
