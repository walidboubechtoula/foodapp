import {Image, StyleSheet, Text, View, Alert} from 'react-native';
import {Card} from 'react-native-ui-lib';
import React from 'react';

interface MenuCardProps {
  intitule: string;
  miniature: string;
  price: string;
  id: string;
  avalible: boolean;
  currency_code: string;
}
export default function MenuCard(props: MenuCardProps) {
  const {intitule, miniature, price, avalible, currency_code, id} = props;
  return (
    <View key={id} style={styles.cardContainer}>
      <Card
        useNative
        enableShadow
        elevation={5}
        style={styles.card}
        activeOpacity={1}
        activeScale={0.96}
        onPress={() => {
          Alert.alert(intitule, 'Vous pouvez faire une commende !');
        }}
        disabled={!avalible}>
        <View style={styles.imageContainer}>
          <Image
            source={{uri: miniature}}
            style={styles.image}
            resizeMode="cover"
          />
          <View style={styles.itemTitleMainContainer}>
            <View style={styles.itemTitleContainer} />
            <Text style={styles.itemTitle}>
              {' '}
              {intitule.length < 35
                ? `${intitule}`
                : `${intitule.substring(0, 28)}...`}
            </Text>
          </View>
          {!avalible && <View style={styles.disable} />}

          <View style={styles.priceContainer}>
            <Text style={styles.price}>
              {price} {currency_code}
            </Text>
          </View>
        </View>
      </Card>
    </View>
  );
}
const styles = StyleSheet.create({
  cardContainer: {
    width: '90%',
    height: 250,
    alignSelf: 'center',
    marginTop: 20,
  },
  card: {
    width: '100%',
    height: '100%',
    borderRadius: 15,
  },
  image: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
  },
  priceContainer: {
    position: 'absolute',
    right: 6,
    top: 6,
    width: 90,
    height: 30,
    borderRadius: 5,
    borderColor: 'red',
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    zIndex: 12,
  },
  price: {
    fontWeight: 'bold',
    color: 'red',
    fontSize: 15,
    textAlign: 'center',
  },
  itemTitle: {
    paddingHorizontal: 10,
    color: '#ffffff',
    fontSize: 17,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  itemTitleMainContainer: {
    position: 'absolute',
    bottom: 0,
    height: 50,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemTitleContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: '#4663b2',
    bottom: 0,
    opacity: 0.7,
  },
  imageContainer: {
    width: '100%',
    height: '100%',
  },
  disable: {
    position: 'absolute',
    bottom: 0,
    height: '100%',
    width: '100%',
    backgroundColor: '#454444',
    opacity: 0.5,
    zIndex: 11,
  },
});
