import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Button} from 'react-native-ui-lib';
import MenuCard from '../components/MenuCard';
import {getData} from './../services/menu.service';
import {DotsLoader} from 'react-native-indicator';
import back from './../../assets/back.png';
import {useNavigation} from '@react-navigation/native';
export default function Menu() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const navigation = useNavigation();
  useEffect(() => {
    setLoading(true);
    getData('RE2')
      .then((res) => {
        setData(res.data.items);
        setLoading(false);
      })
      .catch((err) => {
        Alert.alert(
          'Ooops',
          "Une erreur inattendue s'est produite veuillez essayer ",
        );
        setLoading(false);
      });
  }, []);

  if (loading) {
    return (
      <View style={{flex: 1, alignSelf: 'center', marginTop: 50}}>
        <DotsLoader />
      </View>
    );
  }
  if (data && !loading) {
    return (
      <>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Home');
          }}
          style={styles.head}>
          <Button link style={{position: 'absolute', left: 10}}>
            <Image source={back} style={{width: 25, height: 25}} />
          </Button>
          <Text style={styles.title}>Notre menu</Text>
        </TouchableOpacity>
        <ScrollView>
          {data.map((item: any) => (
            <MenuCard
              intitule={item.title}
              miniature={item.image_url}
              price={item.price}
              avalible={item.availability}
              currency_code={item.currency_code}
              key={item.id}
            />
          ))}
          <View style={{paddingBottom: 100}} />
        </ScrollView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    color: '#003594',
    textAlign: 'center',
    fontFamily: 'Poppins-Bold',
  },
  head: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
