import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
export default function Menu() {
  const navigation = useNavigation();
  return (
    <View style={styles.main}>
      <View style={styles.mainTitleContainer}>
        <Text style={styles.mainTitle}>
          Bien venu dans l'espace digitale de votre restaurant
        </Text>
      </View>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Menu');
        }}
        style={styles.btnContainer}>
        <Text style={[styles.buttonTitle, {}]}>Venez découvrir notre menu</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  main: {
    paddingHorizontal: 25,
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  buttonTitle: {
    color: '#ffffff',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    alignSelf: 'center',
  },
  btnContainer: {
    justifyContent: 'center',
    backgroundColor: '#003594',
    borderRadius: 24,
    height: 40,
    marginTop: 25,
    width: '100%',
  },
  mainTitleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 24,
    marginTop: 25,
    width: '100%',
    height: 100,
  },
  mainTitle: {
    alignSelf: 'center',
    color: '#003594',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    textAlign: 'center',
    fontSize: 22,
  },
});
