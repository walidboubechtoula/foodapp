import API from './api';

export const getData = (id) => {
  return API.get('ya-ios/Eats/1.0.0/restaurant/'.concat(id).concat('/menu'));
};
